const Web3 = require('web3');

window.onload = () => {
    // Variables
    let web3;
    let from;

    // Elements
    const connectButton = document.getElementById('connect');
    const content = document.getElementById('content');
    const account = document.getElementById('account');
    const transactionNumber = document.getElementById("transactionNumber")
    const transactionWrapper = document.getElementById("transactionWrapper")
    const transactionDetails = document.getElementById("transactionDetails")
    const status = document.getElementById("status")

    // Form
    const form = document.getElementById('send');
    const amountInput = document.getElementById('amount');
    const recipientInput = document.getElementById('recipient');

    // Functions
    const connect = async() => {
        if (window.ethereum) {
            web3 = new Web3(window.ethereum);

            try {
                await window.ethereum.request({ method: 'eth_requestAccounts' });

                content.style.display = 'initial';
                connectButton.style.display = 'none';

                const accounts = await web3.eth.getAccounts();

                from = accounts[0];

                account.innerText = from;
            } catch (err) {
                alert('Please accept the request');
            }
        } else {
            alert('Web3 is required');
        }
    };

    const getTransactionDetails = async(result) => {
        transactionNumber.innerHTML = result.transactionHash
        status.innerHTML = "Complete"

        const transactionResponse = await web3.eth.getTransaction(result.transactionHash)
        document.getElementById("from").innerHTML = transactionResponse.from
        document.getElementById("to").innerHTML = transactionResponse.to
        document.getElementById("value").innerHTML = transactionResponse.value
        document.getElementById("gas").innerHTML = transactionResponse.gas
        document.getElementById("gasPrice").innerHTML = transactionResponse.gasPrice
    }

    const transact = async(event) => {
        event.preventDefault();
        const amount = amountInput.value;
        const recipient = recipientInput.value;

        if (!web3.utils.isAddress(recipient)) {
            alert('Dirección inválida');
            return;
        }

        if (Number(amount) <= 0) {
            alert('Cantidad inválida');
            return;
        }

        transactionWrapper.style.display = "initial"
        transactionNumber.innerHTML = "Sending transaction..."
        transactionDetails.style.display = "initial"
        status.innerHTML = "Pending..."

        const result = await web3.eth.sendTransaction({
            from,
            to: recipient,
            value: amount,
        });

        getTransactionDetails(result)
    };

    // Listeners
    connectButton.onclick = connect;
    form.onsubmit = transact;
};